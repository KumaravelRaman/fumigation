package api.retrofit;

import api.models.AttendanceTime.AttendanceReport;
import api.models.ImeiData;
import api.models.Upload.UploadActivity;
import api.models.UploadAudio.AudioUploadResult;
import api.models.Uploadimage.ImageUploadResult;
import api.models.VisitReport.VisitReportData;
import api.models.VisitReport.request.VisitReportReqData;
import api.models.addcontainer.AddContainerData;
import api.models.alertnames.GetAllNameData;
import api.models.attendance.AttendanceData;
import api.models.changepassword.ChangePassword;
import api.models.description.DescriptionData;
import api.models.forgot.ForgotPasswordData;
import api.models.gps.GpsData;
import api.models.jobcard.JobCardData;
import api.models.jobcardcontainer.ContainerData;
import api.models.jobcardera.request.ChemicalsDetails;
import api.models.login.LoginData;
import api.models.loginbranch.LoginBranchData;
import api.models.materialrequest.request.MaterialreqData;
import api.models.materialrequest.response.MaterialData;
import api.models.viewReport.ViewReportData;
import api.models.viewmaterialData.ViewMaterialData;
import api.models.viewmaterialreq.ViewMaterialDataReq;
import api.models.viewreportid.ViewReportIdData;
import api.models.workreport.LocationData;
import api.models.workreport1.request.WorkReportDataValues;
import api.models.workreport1.resposnse.WorkReportresponse;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Created by SYSTEM10 on 6/7/2018.
 */
//Call api service
public interface APIService {
    @FormUrlEncoded
    @POST("API/API/Emailsent")
    public Call<ImeiData> imei(
            @Field("IMINumber1") String imei1,
            @Field("IMINumber2") String imei2);

    @FormUrlEncoded
    @POST("API/API/login")
    public Call<LoginData> login(
            @Field("UserName") String username,
            @Field("Password") String password,
            @Field("IMINumber1") String imei1,
            @Field("IMINumber2") String imei2);
    @FormUrlEncoded
    @POST("API/API/login")
    public Call<LoginBranchData> login1(
            @Field("UserName") String username,
            @Field("Password") String password,
            @Field("UserType") String usertype);
    @FormUrlEncoded
    @POST("API/API/Emailsentgps")
    public Call<GpsData> gps(
            @Field("EmailId") String username);

    @FormUrlEncoded
    @POST("API/API/jobcard")
    public Call<JobCardData> jobcard(
            @Field("id") Integer id
            );

    @FormUrlEncoded
    @POST("API/API/jobcardContainer")
    public Call<ContainerData> container(
            @Field("id") String JobcardId
    );


   /* @Multipart
    @POST("api/Upload/PostUserImage")
    Call<ImageUploadResult> uploadImage(
            @Part MultipartBody.Part filePart
    );*/
   @Multipart
   @POST("api/Upload/PostUserImage")
   Call<ImageUploadResult> uploadImage(@Part MultipartBody.Part file);

    @Multipart
    @POST("api/Upload/PostUserAudio")
    Call<AudioUploadResult> audioimage(
            @Part MultipartBody.Part file);

    @Multipart
    @POST("api/Upload/PostUserAudio")
    Call<AudioUploadResult> audioimage1(
            @Part("name") RequestBody name,
            @Part MultipartBody.Part file);


    @POST("api/API/FumiJobcardFeedback")
    public Call<UploadActivity> upload(
            @Body ChemicalsDetails chemicalmodel);

    @GET("API/API/GetERPList")
    public Call<AttendanceData> attendance();

    @GET("api/API/GetLocationList")
    public Call<LocationData> location();


    @FormUrlEncoded
    @POST("API/API/GetOperaterName")
    public Call<DescriptionData> description(
            @Field("CustomerCode") String id);

    @FormUrlEncoded
    @POST("API/API/Attendance")
    public Call<AttendanceReport> attendnace(
            @Field("EmpId") Integer id,
            @Field("Name") String Name,
            @Field("Date") String date,
            @Field("attendanceTime") String attendancetime,
            @Field("WorkLocation") String location,
            @Field("Address") String address,
            @Field("Designation") String designation,
            @Field("OutAddress") String outaddress,
            @Field("InLatitude") String inlat,
            @Field("InLongitude") String inlong,
            @Field("OutLatitude") String outlat,
            @Field("OutLongitude") String outlong,
            @Field("EmpCode") String code);

    @POST("api/API/FumiMaterialrequest")
    public Call<MaterialData> material(
            @Body MaterialreqData materialdata);

    @FormUrlEncoded
    @POST("api/API/Changepassword")
    public Call<ChangePassword> changepassword(
            @Field("EmpCode") String username,
            @Field("OldPassword") String password,
            @Field("NewPassword") String imei1);
    @FormUrlEncoded
    @POST("api/API/Forgot")
    public Call<ForgotPasswordData> forgot(
            @Field("Customercode") String customercode,
            @Field("email") String email);


    @POST("api/API/FumiJobcardCusVisit")
    public Call<VisitReportData> visit(
           @Body VisitReportReqData visitReportReqData
            );
    @GET("api/API/FumiJobcardCusVisitview")
    public Call<ViewReportData> view();


    @FormUrlEncoded
    @POST("api/API/FumiJobcardCusVisitviewimage")
    public Call<ViewReportIdData> viewreportid(
            @Field("id") Integer id);


    @FormUrlEncoded
    @POST("api/API/ViewMeterialReq")
    public Call<ViewMaterialData> viewmaterial(
            @Field("ReqBranchCode") String branchcode,
            @Field("Status") String status);

    @FormUrlEncoded
    @POST("api/API/ViewMeterialReq1")
    public Call<ViewMaterialDataReq> viewmaterialreq(
            @Field("RequestedNo") String requestno);

    @POST("api/API/AddJobcardEmpReport")
    public Call<WorkReportresponse> workreport(
            @Body WorkReportDataValues workReportDataValues
    );
    @FormUrlEncoded
    @POST("api/API/GetBranchWiseAllName")
    public Call<GetAllNameData> getallname(
            @Field("BranchName") String branchname,
            @Field("BranchCode") String branchcode);
    @FormUrlEncoded
    @POST("api/API/AddContainer")
    public Call<AddContainerData> addcontainer(
            @Field("jobcardid") String jobcardid,
            @Field("ContainerNo") String containernumber);

}
