
package api.models.jobcardcontainer;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ContainerData {

    @SerializedName("data")
    private List<Container> mData;
    @SerializedName("result")
    private String mResult;

    public List<Container> getData() {
        return mData;
    }

    public void setData(List<Container> data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
