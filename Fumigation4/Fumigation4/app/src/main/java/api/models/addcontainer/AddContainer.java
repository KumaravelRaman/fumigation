package api.models.addcontainer;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class AddContainer {
  @SerializedName("LorryDriverMobNo")
  @Expose
  private Object LorryDriverMobNo;
  @SerializedName("Status")
  @Expose
  private Object Status;
  @SerializedName("flg")
  @Expose
  private Integer flg;
  @SerializedName("AudioData")
  @Expose
  private Object AudioData;
  @SerializedName("Updatedate")
  @Expose
  private Object Updatedate;
  @SerializedName("VehicleNo")
  @Expose
  private Object VehicleNo;
  @SerializedName("Images")
  @Expose
  private String Images;
  @SerializedName("Image2")
  @Expose
  private Object Image2;
  @SerializedName("Insertdate")
  @Expose
  private Object Insertdate;
  @SerializedName("Image1")
  @Expose
  private Object Image1;
  @SerializedName("ContainerNo")
  @Expose
  private String ContainerNo;
  @SerializedName("Image")
  @Expose
  private String Image;
  @SerializedName("AudioContentType")
  @Expose
  private Object AudioContentType;
  @SerializedName("chemicals1")
  @Expose
  private Object chemicals1;
  @SerializedName("Remarks")
  @Expose
  private Object Remarks;
  @SerializedName("workreport")
  @Expose
  private Object workreport;
  @SerializedName("warid")
  @Expose
  private Integer warid;
  @SerializedName("vhid")
  @Expose
  private Integer vhid;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("Quatity")
  @Expose
  private Object Quatity;
  @SerializedName("Audio")
  @Expose
  private Object Audio;
  @SerializedName("jobcardid")
  @Expose
  private Integer jobcardid;
  @SerializedName("LorryDriverName")
  @Expose
  private Object LorryDriverName;
  public void setLorryDriverMobNo(Object LorryDriverMobNo){
   this.LorryDriverMobNo=LorryDriverMobNo;
  }
  public Object getLorryDriverMobNo(){
   return LorryDriverMobNo;
  }
  public void setStatus(Object Status){
   this.Status=Status;
  }
  public Object getStatus(){
   return Status;
  }
  public void setFlg(Integer flg){
   this.flg=flg;
  }
  public Integer getFlg(){
   return flg;
  }
  public void setAudioData(Object AudioData){
   this.AudioData=AudioData;
  }
  public Object getAudioData(){
   return AudioData;
  }
  public void setUpdatedate(Object Updatedate){
   this.Updatedate=Updatedate;
  }
  public Object getUpdatedate(){
   return Updatedate;
  }
  public void setVehicleNo(Object VehicleNo){
   this.VehicleNo=VehicleNo;
  }
  public Object getVehicleNo(){
   return VehicleNo;
  }
  public void setImages(String Images){
   this.Images=Images;
  }
  public String getImages(){
   return Images;
  }
  public void setImage2(Object Image2){
   this.Image2=Image2;
  }
  public Object getImage2(){
   return Image2;
  }
  public void setInsertdate(Object Insertdate){
   this.Insertdate=Insertdate;
  }
  public Object getInsertdate(){
   return Insertdate;
  }
  public void setImage1(Object Image1){
   this.Image1=Image1;
  }
  public Object getImage1(){
   return Image1;
  }
  public void setContainerNo(String ContainerNo){
   this.ContainerNo=ContainerNo;
  }
  public String getContainerNo(){
   return ContainerNo;
  }
  public void setImage(String Image){
   this.Image=Image;
  }
  public String getImage(){
   return Image;
  }
  public void setAudioContentType(Object AudioContentType){
   this.AudioContentType=AudioContentType;
  }
  public Object getAudioContentType(){
   return AudioContentType;
  }
  public void setChemicals1(Object chemicals1){
   this.chemicals1=chemicals1;
  }
  public Object getChemicals1(){
   return chemicals1;
  }
  public void setRemarks(Object Remarks){
   this.Remarks=Remarks;
  }
  public Object getRemarks(){
   return Remarks;
  }
  public void setWorkreport(Object workreport){
   this.workreport=workreport;
  }
  public Object getWorkreport(){
   return workreport;
  }
  public void setWarid(Integer warid){
   this.warid=warid;
  }
  public Integer getWarid(){
   return warid;
  }
  public void setVhid(Integer vhid){
   this.vhid=vhid;
  }
  public Integer getVhid(){
   return vhid;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setQuatity(Object Quatity){
   this.Quatity=Quatity;
  }
  public Object getQuatity(){
   return Quatity;
  }
  public void setAudio(Object Audio){
   this.Audio=Audio;
  }
  public Object getAudio(){
   return Audio;
  }
  public void setJobcardid(Integer jobcardid){
   this.jobcardid=jobcardid;
  }
  public Integer getJobcardid(){
   return jobcardid;
  }
  public void setLorryDriverName(Object LorryDriverName){
   this.LorryDriverName=LorryDriverName;
  }
  public Object getLorryDriverName(){
   return LorryDriverName;
  }
}