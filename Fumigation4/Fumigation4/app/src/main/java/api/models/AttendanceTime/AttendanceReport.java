
package api.models.AttendanceTime;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class AttendanceReport {

    @SerializedName("data")
    private report mData;
    @SerializedName("result")
    private String mResult;

    public report getData() {
        return mData;
    }

    public void setData(report data) {
        mData = data;
    }

    public String getResult() {
        return mResult;
    }

    public void setResult(String result) {
        mResult = result;
    }

}
