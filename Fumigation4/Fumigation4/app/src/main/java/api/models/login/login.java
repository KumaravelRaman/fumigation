package api.models.login;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class login {
  @SerializedName("Zip")
  @Expose
  private String Zip;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("InsertDate")
  @Expose
  private String InsertDate;
  @SerializedName("Count")
  @Expose
  private String Count;
  @SerializedName("StuffingPoint")
  @Expose
  private String StuffingPoint;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("SecurityKey")
  @Expose
  private String SecurityKey;
  @SerializedName("UpdateDate")
  @Expose
  private String UpdateDate;
  @SerializedName("Verified")
  @Expose
  private String Verified;
  @SerializedName("IMINumber2")
  @Expose
  private String IMINumber2;
  @SerializedName("IMINumber1")
  @Expose
  private Long IMINumber1;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("BranchName")
  @Expose
  private String BranchName;
  @SerializedName("Password")
  @Expose
  private String Password;
  @SerializedName("MobileNo")
  @Expose
  private String MobileNo;
  @SerializedName("EmailId")
  @Expose
  private String EmailId;
  @SerializedName("AadharNo")
  @Expose
  private String AadharNo;
  @SerializedName("UserName")
  @Expose
  private String UserName;
  @SerializedName("CustomerCode")
  @Expose
  private String CustomerCode;
  @SerializedName("SupervisorName")
  @Expose
  private String SupervisorName;
  @SerializedName("MobileNumber1")
  @Expose
  private Long MobileNumber1;
  @SerializedName("MobileNumber2")
  @Expose
  private String MobileNumber2;
  @SerializedName("City")
  @Expose
  private String City;
  @SerializedName("ERPid")
  @Expose
  private String ERPid;
  @SerializedName("FieldOperatorName")
  @Expose
  private String FieldOperatorName;
  @SerializedName("State")
  @Expose
  private String State;
  @SerializedName("ClientCode")
  @Expose
  private String ClientCode;
  @SerializedName("UserType")
  @Expose
  private String UserType;
  @SerializedName("BranchCode")
  @Expose
  private String BranchCode;
  public void setZip(String Zip){
   this.Zip=Zip;
  }
  public String getZip(){
   return Zip;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setInsertDate(String InsertDate){
   this.InsertDate=InsertDate;
  }
  public String getInsertDate(){
   return InsertDate;
  }
  public void setCount(String Count){
   this.Count=Count;
  }
  public String getCount(){
   return Count;
  }
  public void setStuffingPoint(String StuffingPoint){
   this.StuffingPoint=StuffingPoint;
  }
  public String getStuffingPoint(){
   return StuffingPoint;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setSecurityKey(String SecurityKey){
   this.SecurityKey=SecurityKey;
  }
  public String getSecurityKey(){
   return SecurityKey;
  }
  public void setUpdateDate(String UpdateDate){
   this.UpdateDate=UpdateDate;
  }
  public String getUpdateDate(){
   return UpdateDate;
  }
  public void setVerified(String Verified){
   this.Verified=Verified;
  }
  public String getVerified(){
   return Verified;
  }
  public void setIMINumber2(String IMINumber2){
   this.IMINumber2=IMINumber2;
  }
  public String getIMINumber2(){
   return IMINumber2;
  }
  public void setIMINumber1(Long IMINumber1){
   this.IMINumber1=IMINumber1;
  }
  public Long getIMINumber1(){
   return IMINumber1;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setBranchName(String BranchName){
   this.BranchName=BranchName;
  }
  public String getBranchName(){
   return BranchName;
  }
  public void setPassword(String Password){
   this.Password=Password;
  }
  public String getPassword(){
   return Password;
  }
  public void setMobileNo(String MobileNo){
   this.MobileNo=MobileNo;
  }
  public String getMobileNo(){
   return MobileNo;
  }
  public void setEmailId(String EmailId){
   this.EmailId=EmailId;
  }
  public String getEmailId(){
   return EmailId;
  }
  public void setAadharNo(String AadharNo){
   this.AadharNo=AadharNo;
  }
  public String getAadharNo(){
   return AadharNo;
  }
  public void setUserName(String UserName){
   this.UserName=UserName;
  }
  public String getUserName(){
   return UserName;
  }
  public void setCustomerCode(String CustomerCode){
   this.CustomerCode=CustomerCode;
  }
  public String getCustomerCode(){
   return CustomerCode;
  }
  public void setSupervisorName(String SupervisorName){
   this.SupervisorName=SupervisorName;
  }
  public String getSupervisorName(){
   return SupervisorName;
  }
  public void setMobileNumber1(Long MobileNumber1){
   this.MobileNumber1=MobileNumber1;
  }
  public Long getMobileNumber1(){
   return MobileNumber1;
  }
  public void setMobileNumber2(String MobileNumber2){
   this.MobileNumber2=MobileNumber2;
  }
  public String getMobileNumber2(){
   return MobileNumber2;
  }
  public void setCity(String City){
   this.City=City;
  }
  public String getCity(){
   return City;
  }
  public void setERPid(String ERPid){
   this.ERPid=ERPid;
  }
  public String getERPid(){
   return ERPid;
  }
  public void setFieldOperatorName(String FieldOperatorName){
   this.FieldOperatorName=FieldOperatorName;
  }
  public String getFieldOperatorName(){
   return FieldOperatorName;
  }
  public void setState(String State){
   this.State=State;
  }
  public String getState(){
   return State;
  }
  public void setClientCode(String ClientCode){
   this.ClientCode=ClientCode;
  }
  public String getClientCode(){
   return ClientCode;
  }
  public void setUserType(String UserType){
   this.UserType=UserType;
  }
  public String getUserType(){
   return UserType;
  }
  public void setBranchCode(String BranchCode){
   this.BranchCode=BranchCode;
  }
  public String getBranchCode(){
   return BranchCode;
  }
}