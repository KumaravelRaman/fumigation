package adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brainmagic.fumigation.MapsActivity;
import com.brainmagic.fumigation.R;
import com.brainmagic.fumigation.ViewCoordinate;

import java.util.List;

/**
 * Created by SYSTEM10 on 10/24/2018.
 */

public class ViewCordinateAdapter extends ArrayAdapter<String> {
    //intilize the variable name...
    private Context context;
    private List<String> Timelist,addresslist;



    public ViewCordinateAdapter(@NonNull Context context, List<String> Timelist, List<String>addresslist) {
        super(context, R.layout.viewtrack,Timelist);
        this.context = context;
        this.Timelist = Timelist ;
        this.addresslist = addresslist ;

    }
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        convertView = null;
        if (convertView == null) {
            convertView = ((LayoutInflater) context.getSystemService("layout_inflater")).
                    inflate(R.layout.viewtrack, parent, false);
            //find the variable name...
              TextView time=(TextView)convertView.findViewById(R.id.timeview);
              TextView sno=(TextView)convertView.findViewById(R.id.snoview);
              TextView address=(TextView)convertView.findViewById(R.id.addressview);
              //set the variable name...
              sno.setText(position+1+"");
              time.setText(Timelist.get(position));
            address.setText(addresslist.get(position));


        }


        return convertView;
    }
    private class Jobcarddetailsfumi {

        public TextView sno;
        public TextView name;
        public TextView date;
        public TextView intime;
        public TextView outtime;


    }
}
