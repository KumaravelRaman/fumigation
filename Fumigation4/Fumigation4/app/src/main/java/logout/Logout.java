package logout;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;



import register.LoginActivity;
import toaster.Toasts;


import static android.content.Context.MODE_PRIVATE;

/**
 * Created by Systems02 on 30-Oct-17.
 */

public class Logout {
    private Context context;


    public Logout(Context context) {
        this.context = context;
    }


    public void log_out()
    {
        SharedPreferences myshare = context.getSharedPreferences("fumigation", MODE_PRIVATE);
        SharedPreferences.Editor editor = myshare.edit();

        if(myshare.getBoolean("isLogin",false))
        editor.putBoolean("isLogin", false);
        editor.apply();
        context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
        new Toasts(context).ShowSuccessToast("Logged out successfully");



    }





}
