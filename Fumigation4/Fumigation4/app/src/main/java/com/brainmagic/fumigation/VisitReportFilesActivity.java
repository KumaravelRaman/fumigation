package com.brainmagic.fumigation;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.fxn.pix.Pix;
import com.fxn.utility.Utility;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.jaredrummler.materialspinner.MaterialSpinner;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import cafe.adriel.androidaudiorecorder.AndroidAudioRecorder;
import cafe.adriel.androidaudiorecorder.model.AudioChannel;
import cafe.adriel.androidaudiorecorder.model.AudioSampleRate;
import cafe.adriel.androidaudiorecorder.model.AudioSource;
import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;
import droidninja.filepicker.utils.Orientation;
import nl.changer.audiowife.AudioWife;
import pub.devrel.easypermissions.EasyPermissions;
import uploadService.ProgressRequestBody;

import static com.fxn.utility.Utility.getExifCorrectedBitmap;

public class VisitReportFilesActivity extends AppCompatActivity implements EasyPermissions.PermissionCallbacks,
        ProgressRequestBody.UploadCallbacks {
    //intilize thedeetailss...
    private ImageView image;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private static final int IMAGE_PICKER = 1;
    public static final int RC_AUDIO_REC_PREM = 456;
    private static final int AUIDO_PICKER = 0;
    private ArrayList<String> photoPaths = new ArrayList<>();
    private static final int REQUEST_RECORD_AUDIO = 0;
    private int imageSize;
    private Button mSelectAudio;
    private View mPlayMedia;
    private View mPauseMedia;
    private SeekBar mMediaSeekBar;
    private TextView mRunTime;
    private TextView mTotalTime;
    private LinearLayout mAudio_layout;
    private Button mUploadAudio;
    private Button submit;
    private static final String AUDIO_FILE_PATH =
            Environment.getExternalStorageDirectory().getPath() + "/recorded_audio.mp3";
    private File audio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_report_files);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        image =(ImageView)findViewById(R.id.image1);
        // audio player controll
        mPlayMedia = findViewById(R.id.play_btn);
        mPauseMedia = findViewById(R.id.pause_btn);
        mMediaSeekBar = (SeekBar) findViewById(R.id.media_seekbar);
        mRunTime = (TextView) findViewById(R.id.run_time);
        mTotalTime = (TextView) findViewById(R.id.total_time);
        mAudio_layout = (LinearLayout) findViewById(R.id.audio_layout);

        mSelectAudio = (Button) findViewById(R.id.select_audio);
        mUploadAudio = (Button) findViewById(R.id.upload_audio);
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // Pix.start(VisitReportFilesActivity.this, 100, 1);
                ImagePicker();

            }
        });
        mSelectAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                File oldAudio = new File(AUDIO_FILE_PATH);
                if (oldAudio.exists()) {
                    oldAudio.delete();
                }
                AudioPicker();
            }
        });

        mPlayMedia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio = new File(AUDIO_FILE_PATH);
                if (!audio.exists()) {
                    Toast.makeText(VisitReportFilesActivity.this, "Record an audio file before playing", Toast.LENGTH_LONG).show();
                } else {
                    AudioWife.getInstance()
                            .init(VisitReportFilesActivity.this, Uri.fromFile(audio))
                            .setPlayView(mPlayMedia)
                            .setPauseView(mPauseMedia)
                            .setSeekBar(mMediaSeekBar)
                            .setRuntimeView(mRunTime)
                            .setTotalTimeView(mTotalTime);

                    AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                        @Override
                        public void onCompletion(MediaPlayer mp) {
                            mp.reset();
                        }
                    });
                }
            }
        });
    }
    //Set audio...
    public void AudioPicker() {

        if (EasyPermissions.hasPermissions(this, Manifest.permission.RECORD_AUDIO)) {
            OpenAudioRecorder();
        } else {
            // Ask for one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_audio_recorder), RC_AUDIO_REC_PREM, Manifest.permission.RECORD_AUDIO);
        }

    }

    private void OpenAudioRecorder() {
        AndroidAudioRecorder.with(VisitReportFilesActivity.this)
                // Required
                .setFilePath(AUDIO_FILE_PATH)
                .setColor(ContextCompat.getColor(this, R.color.red))
                .setRequestCode(REQUEST_RECORD_AUDIO)
                // Optional
                .setSource(AudioSource.MIC)
                .setChannel(AudioChannel.STEREO)
                .setSampleRate(AudioSampleRate.HZ_48000)
                .setAutoStart(false)
                .setKeepDisplayOn(true)

                // Start recording
                .record();
        audio = new File(AUDIO_FILE_PATH);
        mAudio_layout.setVisibility(View.VISIBLE);
    }

    private void ImagePicker() {
        FilePickerBuilder.getInstance()
                .setMaxCount(1)
                .setSelectedFiles(photoPaths)
                .setActivityTitle("Please select Images")
                .setActivityTheme(R.style.FilePickerTheme)
                .enableCameraSupport(true)
                .enableVideoPicker(false)
                .showGifs(false)
                .showFolderView(false)
                .enableSelectAll(true)
                .enableImagePicker(true)
                .setCameraPlaceholder(R.drawable.custom_camera)
                .withOrientation(Orientation.UNSPECIFIED)
                .pickPhoto(this, IMAGE_PICKER);

    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IMAGE_PICKER:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    photoPaths = new ArrayList<>();
                    photoPaths.addAll(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA));
                    SetImages(photoPaths);
                }
                break;
            case AUIDO_PICKER:
                if (resultCode == Activity.RESULT_OK) {
                    SetAudio();
                }
        }
    }

    private void SetAudio() {
        audio = new File(AUDIO_FILE_PATH);
        if (!audio.exists()) {
            Toast.makeText(VisitReportFilesActivity.this, "Record an audio file before playing", Toast.LENGTH_LONG).show();
        } else {
            AudioWife.getInstance()
                    .init(VisitReportFilesActivity.this, Uri.fromFile(audio))
                    .setPlayView(mPlayMedia)
                    .setPauseView(mPauseMedia)
                    .setSeekBar(mMediaSeekBar)
                    .setRuntimeView(mRunTime)
                    .setTotalTimeView(mTotalTime);

            AudioWife.getInstance().addOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mp) {
                    mp.reset();
                }
            });
        }
    }

    private void SetImages(ArrayList<String> photoPaths) {
        if (!photoPaths.isEmpty()) {
            if (photoPaths.size() == 1) {
                Glide.with(VisitReportFilesActivity.this)
                        .load(new File(photoPaths.get(0)))
                        .apply(RequestOptions.centerCropTransform()
                                .dontAnimate()
                                .override(imageSize, imageSize)
                                .placeholder(droidninja.filepicker.R.drawable.image_placeholder))
                        .thumbnail(0.5f)
                        .into(image);

                image.setImageResource(R.drawable.ic_add_a_photo_black_24dp);


            }
        } else {
            image.setImageResource(R.drawable.ic_add_a_photo_black_24dp);

        }
    }

}

