package alert;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

/**
 * Created by SYSTEM10 on 4/26/2018.
 */

public class Alertbox {
    Context context;


    public Alertbox(Context con) {
        // TODO Auto-generated constructor stub
        this.context = con;
    }


    public void showAlertbox(String msg)
    {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                context);
        alertDialog.setMessage(msg);
        alertDialog.setTitle("Fumigation");
        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

            }
        });
        //alertDialog.setIcon(R.drawable.logofum);
        alertDialog.show();
    }



    public void showNegativebox(String msg)
    {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(
                context);
        alertDialog.setMessage(msg);
        alertDialog.setTitle("fumigation");
        alertDialog.setPositiveButton("okay", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // TODO Auto-generated method stub

                ((Activity) context).onBackPressed();
            }
        });
        alertDialog.show();
    }
}
