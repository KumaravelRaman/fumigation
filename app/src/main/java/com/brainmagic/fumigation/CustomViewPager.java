package com.brainmagic.fumigation;

/**
 * Created by SYSTEM10 on 12/21/2018.
 */

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.LinearLayout;

public class CustomViewPager extends ViewPager {

    private boolean enabled;

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.enabled = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        if (this.enabled) {
//            return super.onTouchEvent(event);
//        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
//        if (this.enabled) {
//            return super.onInterceptTouchEvent(event);
//        }

        return false;
    }

    public void setPagingEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}




