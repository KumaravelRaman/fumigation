package com.brainmagic.fumigation;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.RemoteException;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.borax12.materialdaterangepicker.time.RadialPickerLayout;
import com.borax12.materialdaterangepicker.time.TimePickerDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import javax.net.ssl.HttpsURLConnection;

import alert.Alertbox;
import api.models.attendance.Attendancename;
import pub.devrel.easypermissions.EasyPermissions;
import toaster.Toasts;

import static android.content.Context.MODE_PRIVATE;

public class ViewReportFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, android.location.LocationListener, com.google.android.gms.location.LocationListener
        ,EasyPermissions.PermissionCallbacks,ResultCallback<LocationSettingsResult>{
    private Button submitte;
    private static EditText ename, mobilenumber;
    private static TextView date,time;
    private EditText placeofvisit;
    private Context context;
    // location
    // Google client to interact with Google API
    private ListView listView;
    private static final int PERMISSION_REQUEST = 100;
    private ImageView menu, back, attendancetime;
    private AlertDialog alertDialog;
    private Alertbox box = new Alertbox(context);
    private Connection connection;
    // private ArrayList<String> empidlist,namelist;
    private static  String datepost1,gettime1;
    private Statement statement;
    private ResultSet resultSet;
    private ProgressDialog loading;
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private MaterialSpinner worklocate;
    private String jobcard, jobcardid, selectname;
    private List<Attendancename> AttendanceData;
    private List<String> locationdata;
    List<Attendancename> namelist;
    private String description;
    double latitude;
    double longitude;
    LocationHelper locationHelper;
    String selectwork;
    private String name, selectid;
    private boolean ischeck = false;
    private List<String> getnameList, getIdlist, getCustomerList;
    private static String fromstring;
    // location
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    private static final String TAG = "";
    private int REQUEST_CHECK_SETTINGS = 100;
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    String address, address1, city, state, country, postalCode, title, selectedempid = "";
    private Button attendancebtn;
    private String employeename, design;
    private String location;
    private Boolean ischeckin;
    private Boolean timing = true;
    private TimePickerDialog timePickerDialog;
    private String customername,mobile,addressget,datepost,gettime;
    private Toasts toasts;
    private String formattedDate,dateformat,formattedTime,formattedTime1,formatedDate2;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_view_report_fragment, container, false);
        myshare = getActivity().getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        context=((VisitReportFragment)getActivity());
        toasts=new Toasts(context);
       // menu=(ImageView)rootView.findViewById(R.id.menu) ;
        //back=(ImageView)rootView.findViewById(R.id.back);
        submitte = (Button)rootView. findViewById(R.id.submitte);
        ename = (EditText) rootView.findViewById(R.id.ename);
        mobilenumber = (EditText)rootView. findViewById(R.id.mobileno);
        date = (TextView) rootView.findViewById(R.id.Date);
        time = (TextView) rootView.findViewById(R.id.time);
        placeofvisit = (EditText)rootView.findViewById(R.id.address);
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        formattedDate = df.format(c.getTime());
        date.setText(formattedDate);

        SimpleDateFormat df3=new SimpleDateFormat("yyyy-MM-dd");
        formatedDate2=df3.format(c.getTime());
        //date.setText(formatedDate2);

        //Time
        SimpleDateFormat df1 = new SimpleDateFormat("hh:mm a");
        formattedTime = df1.format(c.getTime());
        time.setText(formattedTime);

        //Time Server
        SimpleDateFormat df2 = new SimpleDateFormat("hh:mm");
        formattedTime1 = df2.format(c.getTime());


        if (getServicesAvailable()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
            createLocationRequest();
            //  Toast.makeText(this, "Google Service Is Available!!", Toast.LENGTH_SHORT).show();
        }
        isGpsOn();
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment newFragment = new FromDatePickerFragment();
                newFragment.show(getFragmentManager(), "datePicker");
            }
        });

        submitte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(ename.getText().toString().equals("")){
                    toasts.ShowErrorToast("Enter Customer Name");
                }else if(mobilenumber.getText().toString().equals("")){
                    toasts.ShowErrorToast("Enter Mobile Number");
                }else if(placeofvisit.getText().toString().equals("")){
                    toasts.ShowErrorToast("Enter Address");
                }else if(date.getText().toString().equals("")){
                    toasts.ShowErrorToast("Select date");
                }else if(time.getText().toString().equals("")){
                    toasts.ShowErrorToast("Select time");
                }else{
                    customername =ename.getText().toString();
                    mobile=mobilenumber.getText().toString();
                    addressget=placeofvisit.getText().toString();
                    //getdate=date.getText().toString();
                    datepost=datepost;
                    //gettime=time.getText().toString();
                   // viewDetails();
//                    ViewReportFilesFragment viewReportFilesFragment =new ViewReportFilesFragment();
//                    Bundle bundle =new Bundle();
//                    bundle.putString("customername",customername);
//                    bundle.putString("mobilenumber",mobile);
//                    bundle.putString("address",addressget);
//                    bundle.putString("date",getdate);
//                    bundle.putString("time",gettime);
//                    viewReportFilesFragment.setArguments(bundle);
//                    getFragmentManager().beginTransaction().add(R.id.container,viewReportFilesFragment).commit();
                    ((VisitReportFragment)getActivity()).next();
                    ((VisitReportFragment) getActivity()).value(customername, mobile, addressget, formatedDate2, formattedTime1);
                }

            }
        });
//        time.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DialogFragment newFragment = new FromDatePickerFragment1(time);
//                newFragment.show(getFragmentManager(), "Timepicker");

//            }
//        });
        return rootView;
    }

    private void viewDetails() {
    }


    private void isGpsOn() {

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);
        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                final Status status = locationSettingsResult.getStatus();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        startLocationUpdates();
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(getActivity(),
                                    REQUEST_CHECK_SETTINGS);
                        } catch (IntentSender.SendIntentException e) {
                            Log.e(TAG, "Exception : " + e);
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        Log.e(TAG, "Location1 settings are not satisfied.");
                        break;
                }
            }
        });

    }

    public boolean getServicesAvailable() {
        GoogleApiAvailability api = GoogleApiAvailability.getInstance();
        int isAvailable = api.isGooglePlayServicesAvailable(context);
        if (isAvailable == ConnectionResult.SUCCESS) {
            return true;
        } else if (api.isUserResolvableError(isAvailable)) {

            Dialog dialog = api.getErrorDialog(getActivity(), isAvailable, 0);
            dialog.show();
            return false;
        } else {
            Toast.makeText(context, "Cannot Connect To Play Services", Toast.LENGTH_SHORT).show();
            return false;
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();

        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getServicesAvailable()) {
            buildGoogleApiClient();
            if (mGoogleApiClient.isConnected() && mRequestingLocationUpdates) {
                // mGoogleApiClient.stopAutoManage(context);
                startLocationUpdates();
            }
        }


    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient != null)
            if (mGoogleApiClient.isConnected()) {
                mGoogleApiClient.disconnect();
            }
    }

    @Override
    public void onPause() {
        super.onPause();
    }
/*
    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  stopService(new Intent(PlacePickerActivity.this, GPSTracker.class));
        stopLocationUpdates();


    }*/

    // Creating google api client object
    protected synchronized void buildGoogleApiClient() {
        try {
            mGoogleApiClient = new GoogleApiClient.Builder(context)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .enableAutoManage((FragmentActivity) context, this).build();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //Creating location request object
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(AppConstants.UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(AppConstants.FATEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setSmallestDisplacement(AppConstants.DISPLACEMENT);
    }


    //Stopping location updates
    protected void stopLocationUpdates() {
        LocationServices.FusedLocationApi.removeLocationUpdates(
                mGoogleApiClient, this);
    }


    //Ask permissions
    public void AskLocationPermission() {
        if (EasyPermissions.hasPermissions(context, Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Have permission, do the thing!
            if (CheckLocationIsEnabled()) {
                // if location is enabled show place picker activity to use
                startLocationUpdates();

            } else {

                LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                        .addLocationRequest(mLocationRequest);

                PendingResult<LocationSettingsResult> result =
                        LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
                result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                    @Override
                    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {
                        final Status status = locationSettingsResult.getStatus();
                        switch (status.getStatusCode()) {
                            case LocationSettingsStatusCodes.SUCCESS:
                                startLocationUpdates();
                                break;
                            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                                try {
                                    status.startResolutionForResult(
                                            getActivity(),
                                            REQUEST_CHECK_SETTINGS);
                                } catch (IntentSender.SendIntentException e) {
                                    Log.e(TAG, "Exception : " + e);
                                }
                                break;
                            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                                Log.e(TAG, "Location1 settings are not satisfied.");
                                break;
                        }
                    }
                });


            }
        } else {
            // Request one permission
            EasyPermissions.requestPermissions(this, getString(R.string.rationale_ask_again), MY_PERMISSIONS_REQUEST_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION);


        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        // Forward results to EasyPermissions
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> list) {

        if (CheckLocationIsEnabled()) {
            // if location is enabled show place picker activity to user
            startLocationUpdates();
        } else {
            // if location is not enabled show request to enable location to user
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(mLocationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(
                            mGoogleApiClient,
                            builder.build()

                    );
            result.setResultCallback(this);
        }

    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> list) {
        // Some permissions have been denied
        // if location is enabled show place picker activity to user
        // AskLocationPermission();
        if (mRequestingLocationUpdates) {
            startLocationUpdates();
        }
    }


    public void onResult(@NonNull LocationSettingsResult locationSettingsResult) {


        final Status status = locationSettingsResult.getStatus();
        switch (status.getStatusCode()) {
            case LocationSettingsStatusCodes.SUCCESS:
                // NO need to show the dialog;
                ConvertToAddress();
                break;

            case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                //  Location1 settings are not satisfied. Show the user a dialog
                try {
                    // Show the dialog by calling startResolutionForResult(), and check the result
                    // in onActivityResult().
                    status.startResolutionForResult(getActivity(), REQUEST_CHECK_SETTINGS);
                } catch (IntentSender.SendIntentException e) {
                    //failed to show
                }
                break;
            case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                // Location1 settings are unavailable so not possible to show any dialog now
                break;
        }
    }

    private void ConvertToAddress() {
        try {
            new GeocodeAsyncTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
        }catch(Exception e){
            e.printStackTrace();
        }



    }




    private class GeocodeAsyncTask extends AsyncTask<Double, Void, Address> {

        String errorMessage = "";


        @Override
        protected Address doInBackground(Double... latlang) {
            Geocoder geocoder = new Geocoder(context, Locale.getDefault());
            List<Address> addresses = null;
            if (geocoder.isPresent()) {
                try {
                    addresses = geocoder.getFromLocation(latlang[0], latlang[1], 1);
                } catch (IOException ioException) {
                    errorMessage = "Service Not Available";
                    Log.e(TAG, errorMessage, ioException);
                } catch (IllegalArgumentException illegalArgumentException) {
                    errorMessage = "Invalid Latitude or Longitude Used";
                    Log.e(TAG, errorMessage + ". " +
                            "Latitude = " + latlang[0] + ", Longitude = " +
                            latlang[1], illegalArgumentException);
                }

                if (addresses != null && addresses.size() > 0)
                    return addresses.get(0);
            } else {
                new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            }


            return null;
        }

        protected void onPostExecute(Address addresss) {

            if (addresss == null) {
                new GetGeoCodeAPIAsynchTask().execute(mLastLocation.getLatitude(), mLastLocation.getLongitude());
            } else {
                String address = addresss.getAddressLine(0); //0 to obtain first possible address
                String city = addresss.getLocality();
                String state = addresss.getAdminArea();
                //create your custom title
                String title = city + "-" + state;
                placeofvisit.setText(address +
                        "\n"
                        + title);
                editor.putString("address",address +
                        "\n"
                        + title);
                editor.commit();
            }
        }
    }

    private class GetGeoCodeAPIAsynchTask extends AsyncTask<Double, Void, String[]> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String[] doInBackground(Double... latlang) {
            String response;
            try {
                String URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=" + latlang[0] + "," + latlang[1];
                Log.v("URL", URL);
                response = getLatLongByURL(URL);
                return new String[]{response};
            } catch (Exception e) {
                return new String[]{"error"};
            }
        }

        @Override
        protected void onPostExecute(String... result) {
            try {
                JSONObject jsonObject = new JSONObject(result[0]);

                String address = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(0).getString("long_name");

                String city = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(2).getString("long_name");

                String state = ((JSONArray) jsonObject.get("results")).getJSONObject(0)
                        .getJSONArray("address_components").getJSONObject(4).getString("long_name");

                String title = city + "-" + state;


                placeofvisit.setText(address + "\n" + title);

                Log.d("Address", "" + address);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public String getLatLongByURL(String requestURL) {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setRequestProperty("Content-Type",
                    "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response += line;
                }
            } else {
                response = "";
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }

    //Starting the location updates
    protected void startLocationUpdates() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
                mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                ConvertToAddress();
            } else {
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
                Log.d(TAG, "Permission Not Granted");

            }

        } else {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
            ConvertToAddress();

        }
    }

    private boolean CheckLocationIsEnabled() {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }
        if (mGoogleApiClient != null)
            mLastLocation = LocationServices.FusedLocationApi
                    .getLastLocation(mGoogleApiClient);
        if (mLastLocation == null) {
            return false;
        } else {

            return false;
        }
    }


    private void getAddress() {
        Address locationAddress;

        locationAddress = locationHelper.getAddress(latitude, longitude);

        if (locationAddress != null) {

            String address = locationAddress.getAddressLine(0);


            String currentLocation;

            if (!TextUtils.isEmpty(address)) {
                currentLocation = address;

                if (!TextUtils.isEmpty(address1))
                    currentLocation += "\n" + address1;

                if (!TextUtils.isEmpty(city)) {
                    currentLocation += "\n" + city;

                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += " - " + postalCode;
                } else {
                    if (!TextUtils.isEmpty(postalCode))
                        currentLocation += "\n" + postalCode;
                }

                if (!TextUtils.isEmpty(state))
                    currentLocation += "\n" + state;

                if (!TextUtils.isEmpty(country))
                    currentLocation += "\n" + country;


            }

        } else
            Toast.makeText(context, "Something went wrong", Toast.LENGTH_LONG).show();
        //showToast("Something went wrong");

    }




    public void onReceiveResult(@NonNull LocationSettingsResult locationSettingsResult) throws RemoteException {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    public static class FromDatePickerFragment extends DialogFragment implements
            DatePickerDialog.OnDateSetListener {



        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);


            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {

            date.setText(String.format("%d/%d/%d", day, month + 1, year));
            datepost1= (String.format("%d/%d/%d",day,month +1 ,year));

            fromstring = year + "-" + (month + 1) + "-" + day;
            date.clearFocus();

        }
    }


    @SuppressLint("ValidFragment")
    public static class FromDatePickerFragment1 extends DialogFragment implements android.app.TimePickerDialog.OnTimeSetListener {
     private TextView time;
        public FromDatePickerFragment1(TextView editText) {
            this.time = editText;
        }
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {

            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);
            int format =c.get(Calendar.AM_PM);

            return new android.app.TimePickerDialog(getActivity(), this, hour, minute, DateFormat.is24HourFormat(getActivity()));
        }


        @Override
        public void onTimeSet(TimePicker timePicker, int hourofDay, int minute) {
            String AM_PM = " AM";
            String mm_precede = "";
            if (hourofDay >= 12) {
                AM_PM = " PM";
                if (hourofDay >=13 && hourofDay < 24) {
                    hourofDay -= 12;
                }
                else {
                    hourofDay = 12;
                }
            } else if (hourofDay == 0) {
                hourofDay = 12;
            }
            if (minute < 10) {
                mm_precede = "0";
            }
            time.setText(Integer.toString(hourofDay) + ":" + Integer.toString(minute) + AM_PM );
            gettime1=(Integer.toString(hourofDay) + ":" + Integer.toString(minute));

        }
    }
}

