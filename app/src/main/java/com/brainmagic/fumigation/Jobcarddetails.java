package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

import adapter.JobCardAdapter;
import alert.Alertbox;
import api.models.jobcard.JobCard;
import api.models.jobcard.JobCardData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Jobcarddetails extends AppCompatActivity {
    //intialize the Details....
    private ListView listView;
    private JobCardAdapter jobCardAdapter;
    private ImageView menu,back;
    private Alertbox box = new Alertbox(this);
    //intialize the local Storage...
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private List<JobCard>data;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jobcarddetails);
        menu=(ImageView)findViewById(R.id.menu) ;
        //local Database Declaration
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        listView=(ListView)findViewById(R.id.list_item) ;
        back=(ImageView)findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(Jobcarddetails.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(Jobcarddetails.this).log_out();
                                return true;
                            case R.id.attendance:
                                Intent i=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i);
                                return true;
                            case  R.id.home:
                                Intent i5=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i5);
                                return true;
                            case R.id.viewvisit:
                                Intent i6=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;

                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menujobcard);
                popupMenu.show();
                ;
            }
        });
        //Assign Methods
        CheckInternet();
    }

    private void CheckInternet() {
        NetworkConnection isnet = new NetworkConnection(Jobcarddetails.this);
        if (isnet.CheckInternet()) {
            //Assign Methods
            PendingComplaints();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }

    private void PendingComplaints() {
        try {
            final ProgressDialog loading = ProgressDialog.show(Jobcarddetails.this, "Fumigation", "Loading...", false, false);
            APIService service = RetrofitClient.getApiService();
            Call<JobCardData> call = service.jobcard(myshare.getInt("id",0));
            call.enqueue(new Callback<JobCardData>() {
                @Override
                public void onResponse(Call<JobCardData> call, Response<JobCardData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data = response.body().getData();
                            if(data.size() ==0){
                                final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                        Jobcarddetails.this).create();

                                LayoutInflater inflater = getLayoutInflater();
                                View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                                alertDialogbox.setView(dialogView);

                                TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                                Button okay = (Button) dialogView.findViewById(R.id.okay);
                                log.setText("No Jobcard");
                                okay.setOnClickListener(new View.OnClickListener() {

                                    @Override
                                    public void onClick(View arg0) {
                                        // TODO Auto-generated method stub
                                        Intent go = new Intent(Jobcarddetails.this, SupervisorHomeActivity.class);
                                        go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(go);
                                        alertDialogbox.dismiss();
                                        finish();
                                    }
                                });
                                alertDialogbox.show();
                            }else{
                                //Get the Data assign to the adapter............
                                jobCardAdapter = new JobCardAdapter(Jobcarddetails.this, data);
                                listView.setAdapter(jobCardAdapter);
                            }




                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox(getResources().getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<JobCardData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
