package com.brainmagic.fumigation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;

import java.util.List;

import adapter.ContainerDetailsAdapter;
import adapter.ViewVisitAdapter;
import alert.Alertbox;
import api.models.jobcardcontainer.ContainerData;
import api.models.viewReport.ViewData;
import api.models.viewReport.ViewReportData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewReportActivity extends AppCompatActivity {
    private ListView listView;
    private Alertbox box = new Alertbox(this);
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private ViewVisitAdapter viewVisitAdapter;
    private List<ViewData>data;
    private ImageView back;
    private ImageView menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_report);
        menu = (ImageView) findViewById(R.id.menu);
        listView=(ListView)findViewById(R.id.listreport);
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        back=(ImageView)findViewById(R.id.back) ;
        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ViewReportActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(ViewReportActivity.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.attendance:
                                Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i3);
                                return true;
                            case R.id.jobcard:
                                Intent i4=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i4);
                                return true;
                            case R.id.visitreport:
                                Intent i5=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i5);
                                return true;
                            case  R.id.home:
                                Intent i6=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i6);
                                return true;


                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menuviewvisit);
                popupMenu.show();
                ;
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        checkInternet();

    }

    private void checkInternet() {
        NetworkConnection networkConnection =new NetworkConnection(ViewReportActivity.this);
        if(networkConnection.CheckInternet()){
            viewreport();
        }else{
            box.showAlertbox("No Internet Connection");
        }
    }

    private void viewreport() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ViewReportActivity.this, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<ViewReportData> call = service.view();
            call.enqueue(new Callback<ViewReportData>() {
                @Override
                public void onResponse(Call<ViewReportData> call, Response<ViewReportData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            data = response.body().getData();
                            viewVisitAdapter =new ViewVisitAdapter(ViewReportActivity.this,data);
                            listView.setAdapter(viewVisitAdapter);

                        } else if (response.body().getResult().equals("NotSuccess")) {
                            //box.showAlertbox("No Container Details");
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                    ViewReportActivity.this).create();

                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertbox, null);
                            alertDialogbox.setView(dialogView);

                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("No Visit Report");
                            okay.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    Intent go = new Intent(ViewReportActivity.this, SupervisorHomeActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                    finish();
                                }
                            });
                            alertDialogbox.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox(getResources().getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<ViewReportData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(getResources().getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    }
