package com.brainmagic.fumigation;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.pdf.PdfDocument;
import android.os.AsyncTask;
import android.os.Environment;
import java.io.FileOutputStream;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import adapter.ContainerDetailsAdapter;
import alert.Alertbox;
import api.models.addcontainer.AddContainer;
import api.models.addcontainer.AddContainerData;
import api.models.jobcardcontainer.Container;
import api.models.jobcardcontainer.ContainerData;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import logout.Logout;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import toaster.Toasts;

public class ContainerDetailsActivity extends AppCompatActivity {
    //intialize the Details....
    private ListView listView;
    private ContainerDetailsAdapter containerDetailsAdapter;
    private ImageView menu, back, pdf;
    private AlertDialog alertDialog;
    private Alertbox box = new Alertbox(this);
    //intialize the local Storage...
    private SharedPreferences myshare;
    private SharedPreferences.Editor editor;
    private List<Container> Data;
    private String jobcard, jobcardid;
    private ImageView addcontainer;
    private AddContainer addContainer;
    private Toasts toasts = new Toasts(this);;
   // String itemscount = containerDetailsAdapter.getCount();

    List<Bitmap> bmps = new ArrayList<Bitmap>();
    int allitemsheight = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container_details);
        menu = (ImageView) findViewById(R.id.menu);
       // pdf = (ImageView) findViewById(R.id.pdf);
        back = (ImageView) findViewById(R.id.back);
        addcontainer = (ImageView) findViewById(R.id.addcontainer);
        //local Database Declaration
        myshare = getSharedPreferences("fumigation", MODE_PRIVATE);
        editor = myshare.edit();
        jobcard = getIntent().getStringExtra("jobcardno");
        jobcardid = getIntent().getStringExtra("id");
        listView = (ListView) findViewById(R.id.list_item);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        addcontainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowAlertbox(jobcardid,jobcard);
            }
        });
        menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(ContainerDetailsActivity.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(ContainerDetailsActivity.this).log_out();
                                return true;
                            case R.id.attendance:
                                Intent i=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i);
                                return true;
                            case  R.id.home:
                                Intent i5=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i5);
                                return true;
                            case R.id.viewvisit:
                                Intent i6=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i6);
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.visitreport:
                                Intent i4=new Intent(getApplicationContext(), VisitReportFragment.class);
                                startActivity(i4);
                                return true;

                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menujobcard);
                popupMenu.show();
                ;
            }
        });
       /* pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    Generatepdf();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });*/
       //assign the method.....
        CheckInternet();
    }

    private void ShowAlertbox(String s, String jobcardid) {
        alertDialog = new AlertDialog.Builder(
                ContainerDetailsActivity.this).create();

        LayoutInflater inflater = (ContainerDetailsActivity.this).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.updateadd, null);
        alertDialog.setView(dialogView);

        final TextView jobcardnumber  = (TextView) dialogView.findViewById(R.id.jobcardNumber);
        final EditText Conatinernumber = (EditText) dialogView.findViewById(R.id.container);
        Button Save = (Button)  dialogView.findViewById(R.id.save);
        Button Cancel = (Button)  dialogView.findViewById(R.id.cancel);

        jobcardnumber.setText(jobcardid);
        Save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // TODO Auto-generated method stub
                if(Conatinernumber.getText().toString().equals("")){
                    toasts.ShowErrorToast("Enter Container");
                }else{
                    AddContainer(s,Conatinernumber.getText().toString(),alertDialog);
                }


            }
        });
        alertDialog.show();
        Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                alertDialog.dismiss();
            }
        });

    }

    private void AddContainer(String s, String s1, AlertDialog alertDialog) {
        try {
            final ProgressDialog loading = ProgressDialog.show(ContainerDetailsActivity.this, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<AddContainerData> call = service.addcontainer(s,s1);
            call.enqueue(new Callback<AddContainerData>() {
                @Override
                public void onResponse(Call<AddContainerData> call, Response<AddContainerData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            addContainer = response.body().getData();
                            containerdeatls();
                            alertDialog.dismiss();

                        } else if (response.body().getResult().equals("Already jobcardNo and containerNo found")) {
                            //box.showAlertbox("No Container Details");
                            toasts.ShowErrorToast("Already containerNumber found");
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox("Some Datatype Changed");

                    }
                }


                @Override
                public void onFailure(Call<AddContainerData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox("Failure Does Not Fetch The Data");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void CheckInternet() {
        NetworkConnection isnet = new NetworkConnection(ContainerDetailsActivity.this);
        if (isnet.CheckInternet()) {
            //assign the method........
            containerdeatls();
        } else {
            box.showAlertbox(getResources().getString(R.string.no_internet));
        }
    }
    //asssign the method...
    private void containerdeatls() {
        try {
            final ProgressDialog loading = ProgressDialog.show(ContainerDetailsActivity.this, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<ContainerData> call = service.container(jobcardid);
            call.enqueue(new Callback<ContainerData>() {
                @Override
                public void onResponse(Call<ContainerData> call, Response<ContainerData> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            Data = response.body().getData();
                            //Get the data assign the adapter.....
                            containerDetailsAdapter = new ContainerDetailsAdapter(ContainerDetailsActivity.this, Data);
                            listView.setAdapter(containerDetailsAdapter);

                        } else if (response.body().getResult().equals("NotSuccess")) {
                            //box.showAlertbox("No Container Details");
                            final AlertDialog alertDialogbox = new AlertDialog.Builder(
                                    ContainerDetailsActivity.this).create();

                            LayoutInflater inflater = getLayoutInflater();
                            View dialogView = inflater.inflate(R.layout.alertboxlinear, null);
                            alertDialogbox.setView(dialogView);

                            TextView log = (TextView) dialogView.findViewById(R.id.textView1);
                            Button okay = (Button) dialogView.findViewById(R.id.okay);
                            log.setText("No Container Details");
                            okay.setOnClickListener(new View.OnClickListener() {

                                @Override
                                public void onClick(View arg0) {
                                    // TODO Auto-generated method stub
                                    Intent go = new Intent(ContainerDetailsActivity.this, SupervisorHomeActivity.class);
                                    go.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(go);
                                    alertDialogbox.dismiss();
                                    finish();
                                }
                            });
                            alertDialogbox.show();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox("Some Datatype Changed");

                    }
                }


                @Override
                public void onFailure(Call<ContainerData> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox("Failure Does Not Fetch The Data");
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

