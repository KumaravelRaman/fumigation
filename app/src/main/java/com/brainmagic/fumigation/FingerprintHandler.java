package com.brainmagic.fumigation;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.hardware.fingerprint.FingerprintManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CancellationSignal;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.muddzdev.styleabletoastlibrary.StyleableToast;
import com.shashank.sony.fancydialoglib.Animation;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import alert.Alertbox;
import api.models.AttendanceTime.AttendanceReport;
import api.models.AttendanceTime.report;
import api.retrofit.APIService;
import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import service.BGService;
import service.BGServicenormal;

import static android.content.Context.MODE_PRIVATE;
import static android.net.wifi.WifiConfiguration.Status.strings;

/**
 * Created by whit3hawks on 11/16/16.
 */
@RequiresApi(api = Build.VERSION_CODES.M )
public class FingerprintHandler extends FingerprintManager.AuthenticationCallback {

    private Context context;
    private SharedPreferences sPreferences;
    private SharedPreferences.Editor editor;
     String name;
     String description,address,locate,lat,lon;
    Integer empid;
    String formattedtime,formattedDate,ischeck;
    private Boolean isCheckedIn=false;
    private Boolean ischeckbool,istiming,islinit,servuce;
    private Alertbox box;
    private report Data;
    private RunBackgroundService backgroundService;
    String code;
    private static final String TAG = "FingerprintHandler";

    // Constructor
    public FingerprintHandler(Context mContext,RunBackgroundService runBackgroundService) {
        box = new Alertbox(context);
        this.context = mContext;
        this.backgroundService=runBackgroundService;


        //Time
        Calendar cal = Calendar.getInstance();
        Date currentTime = cal.getTime();
        SimpleDateFormat dftime = new SimpleDateFormat("HH:mm:ss");
        formattedtime= dftime.format(currentTime);

        //date
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        formattedDate = df.format(c.getTime());



    }
    @RequiresApi(api = Build.VERSION_CODES.M)
    public void startAuth(FingerprintManager manager, FingerprintManager.CryptoObject cryptoObject) {
        CancellationSignal cancellationSignal = new CancellationSignal();
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.USE_FINGERPRINT) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        manager.authenticate(cryptoObject, cancellationSignal, 0, this, null);
    }

    @Override
    public void onAuthenticationError(int errMsgId, CharSequence errString) {
        this.update("Fingerprint Authentication error\n" + errString);
    }

    @Override
    public void onAuthenticationHelp(int helpMsgId, CharSequence helpString) {
        this.update("Fingerprint Authentication help\n" + helpString);
    }

    @Override
    public void onAuthenticationFailed() {

        showalertbox1();

        //this.update("Fingerprint Authentication failed.");
    }

    private void showalertbox1() {
        new FancyAlertDialog.Builder((Activity) context)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Fingerprint Authentication failed.")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.crosssymbol, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();

    }

    @Override
    public void onAuthenticationSucceeded(FingerprintManager.AuthenticationResult result) {
        //((Activity) context).finish();
        sPreferences=context.getSharedPreferences("fumigation",MODE_PRIVATE);
        editor = sPreferences.edit();
        istiming =sPreferences.getBoolean("istiming",true);
        islinit =sPreferences.getBoolean("islinit",true);
        name=sPreferences.getString("name","");
        description=sPreferences.getString("usertype","");
        address=sPreferences.getString("ToAddress","");
        locate=sPreferences.getString("worklocation","");
        empid=sPreferences.getInt("id",0);
        lat=sPreferences.getString("latitude","");
        lon=sPreferences.getString("longitude","");
        isCheckedIn=sPreferences.getBoolean("isCheckedIn",isCheckedIn);
        code=sPreferences.getString("customercode","");
       //isCheckedIn=sPreferences.getBoolean("isCheckedIn",isCheckedIn);

        checkInternet();


     /*   new MaterialDialog.Builder(context)
                .title("Fumigation")
                .message("Attendancename Successfully")
                .positiveText("OK")
                .neutralText("NOT NOW")
                .negativeText("CANCEL")
                .positiveColor(R.color.green_700)
                .neutralColor(R.color.yellow_700)
                .negativeColor(R.color.pink_700)
                .buttonCallback(new MaterialDialog.ButtonCallback() {
                    @Override
                    public void onPositive(MaterialDialog dialog) {
                        super.onPositive(dialog);
                        Toast.makeText(context, "OK", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onNeutral(MaterialDialog dialog) {
                        super.onNeutral(dialog);
                        Toast.makeText(context, "Not now", Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onNegative(MaterialDialog dialog) {
                        super.onNegative(dialog);
                        Toast.makeText(context, "Cancel", Toast.LENGTH_SHORT).show();
                    }
                })
                .show();*/

    }

    private void checkInternet() {
        NetworkConnection isnet = new NetworkConnection(context);
        if (isnet.CheckInternet()) {

            if (!isCheckedIn) {
                new Upload().execute("Start","");

            } else {
                new Upload().execute("stop","");
                //inflateAlertBox();
            }
        }
        else{
                box.showAlertbox(context.getString(R.string.no_internet));
            }
        }

    private void inflateAlertBox() {
        new FancyAlertDialog.Builder((Activity) context)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Your Current Time "+formattedtime+" is marked as your  Out Time Attendance")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.checked, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        new Upload().execute("stop","");
                        Intent i=new Intent(context, AttendanceCrashActivity.class);
                        context.startActivity(i);
                        ((Activity) context).finish();
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }


    private void upload() {
        try {

            final ProgressDialog loading = ProgressDialog.show(context, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<AttendanceReport> call = service.attendnace(empid,name,formattedDate,formattedtime,locate,address,description,address,lat,lon,lat,lon,code);
            call.enqueue(new Callback<AttendanceReport>() {
                @Override
                public void onResponse(Call<AttendanceReport> call, Response<AttendanceReport> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                            Data = response.body().getData();
                            if(response.body().getResult().equals("Success")){
                                editor.putInt("trackid",response.body().getData().getEmpId());
                                editor.putInt("sid",response.body().getData().getId());
                               context.startService(new Intent(context, BGService.class));
                                showalertbo(formattedtime);
                            }else if(response.body().getResult().equals("Already Punched")){
                                showalert();
                            }

                          /* Intent i =new Intent(context,AttendanceHomeActivity.class);
                           context.startActivity(i);*/


                        } else if (response.body().getResult().equals("NotSuccess")) {
                            box.showAlertbox( context.getString(R.string.server_error));
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox( context.getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<AttendanceReport> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(context.getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showalertbo(String formattedtime) {
        new FancyAlertDialog.Builder((Activity) context)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Your Current Time "+formattedtime+" is marked as your  Out Time Attendance")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.checked, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Intent i=new Intent(context, AttendanceCrashActivity.class);
                        context.startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();

    }


    private void Uploadvalues() {
        try {

            final ProgressDialog loading = ProgressDialog.show(context, "Fumigation", "Loading...", false, false);
            final APIService service = RetrofitClient.getApiService();
            Call<AttendanceReport> call = service.attendnace(empid,name,formattedDate,formattedtime,locate,address,description,address,lat,lon,lat,lon,code);
            call.enqueue(new Callback<AttendanceReport>() {
                @Override
                public void onResponse(Call<AttendanceReport> call, Response<AttendanceReport> response) {
                    try {
                        loading.dismiss();
                        if (response.body().getResult().equals("Success")) {
                           Data = response.body().getData();
                           if(response.body().getResult().equals("Success")){
                               context.startService(new Intent(context, BGService.class));
                               showalertbox(formattedtime);
                           }else if(response.body().getResult().equals("Already Punched")){
                                showalert();
                           }

                          /* Intent i =new Intent(context,AttendanceHomeActivity.class);
                           context.startActivity(i);*/


                        } else if (response.body().getResult().equals("Already Punched")) {
                            showalert();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        loading.dismiss();
                        box.showAlertbox( context.getString(R.string.server_error));

                    }
                }


                @Override
                public void onFailure(Call<AttendanceReport> call, Throwable t) {
                    loading.dismiss();
                    t.printStackTrace();
                    box.showAlertbox(context.getString(R.string.server_error));
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showalert() {
        new FancyAlertDialog.Builder((Activity) context)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("You have already Punched Today")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.cancel, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Intent i=new Intent(context, AttendanceCrashActivity.class);
                        context.startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();
    }

    private void showalertbox(String formattedtime) {
        new FancyAlertDialog.Builder((Activity) context)
                .setTitle("Fumigation Services Field Force")
                .setBackgroundColor(Color.parseColor("#303F9F"))  //Don't pass R.color.colorvalue
                .setMessage("Your Current Time "+formattedtime+" is marked as your In Time Attendance")
                .setNegativeBtnText("Cancel")
                .setPositiveBtnBackground(Color.parseColor("#FF4081"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("Ok")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(Animation.SIDE)
                .isCancellable(false)
                .setIcon(R.drawable.checked, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Intent i=new Intent(context, AttendanceCrashActivity.class);
                        context.startActivity(i);
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {

                    }
                })
                .build();

    }


    private void update(String e){
        TextView textView = (TextView) ((Activity)context).findViewById(R.id.errorText);
        textView.setText(e);
    }

    private class Upload  extends AsyncTask<String,Void,String>{
        final ProgressDialog loading = ProgressDialog.show(context, "Fumigation", "Loading...", false, false);

            @Override
            protected void onPreExecute() {
            super.onPreExecute();
            }

            @Override
            protected String doInBackground(String... strings) {
            final String string=strings[0];
            try {
                final APIService service = RetrofitClient.getApiService();
                Call<AttendanceReport> call = service.attendnace(empid,name,formattedDate,formattedtime,locate,address,description,address,lat,lon,lat,lon,code);
                call.enqueue(new Callback<AttendanceReport>() {
                    @Override
                    public void onResponse(Call<AttendanceReport> call, Response<AttendanceReport> response) {
                        if (response.body().getResult().equals("Success")) {
                            int trackId=(response.body().getData().getId());
                            int s_Id=(response.body().getData().getEmpId());
                            editor.putInt("trackid",trackId);
                            editor.putInt("sid",s_Id);

                            if(string.equals("Start")) {
                                try {
                                    editor.putString("fingerType","fingerPrint");
                                    context.startService(new Intent(context, BGService.class));
//                                    backgroundService.runBackgroundService(string);
                                    showalertbox(formattedtime);
                                    istiming = true;
                                    servuce=false;
                                    isCheckedIn = true;
                                    editor.putBoolean("isCheckedIn", isCheckedIn);
                                    editor.putBoolean("service",servuce);
                                    editor.putBoolean("istiming", istiming);
                                    editor.putString("formateddate", formattedDate);
                                    editor.putString("formatedtime", formattedtime);
                                    editor.commit();
                                    String phoneType=sPreferences.getString("fingerType","");
                                    Log.d(TAG, "onResponse: "+phoneType);
                                }catch (Exception e)
                                {
                                    e.printStackTrace();
                                }

                            }
                            else if(string.equals("stop"))
                            {
//                                backgroundService.runBackgroundService(string);
                                context.stopService(new Intent(context, BGService.class));
                                isCheckedIn = false;
                                islinit = true;
                                istiming = false;
                                servuce=false;
                                inflateAlertBox();
                                editor.putBoolean("isCheckedIn", isCheckedIn);
                                editor.putBoolean("istiming", istiming);
                                editor.putBoolean("islinit", islinit);
                                editor.putBoolean("service",servuce);
                                editor.putString("formateddate", formattedDate);
                                editor.putString("formatedtime", formattedtime);
                                editor.commit();
                            }

                        } else if (response.body().getResult().equals("Already Punched")) {
                            editor.putString("formateddate", formattedDate);
                            editor.commit();
                            showalert();
//                            Toast.makeText(getApplicationContext(), "Already Punched", Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "Please try again later...", Toast.LENGTH_SHORT).show();
                        }
                        loading.dismiss();
                    }

                    @Override
                    public void onFailure(Call<AttendanceReport> call, Throwable t) {
                        loading.dismiss();
                        Toast.makeText(context, "Attempt Failed. Please try again later...", Toast.LENGTH_SHORT).show();
                    }
                });
            }catch (Exception e){
                loading.dismiss();
                Toast.makeText(context, "Server not responding...", Toast.LENGTH_SHORT).show();
            }
            loading.dismiss();
            return "";
        }

        }
    public interface RunBackgroundService {
        void runBackgroundService(String start);
    }
}
