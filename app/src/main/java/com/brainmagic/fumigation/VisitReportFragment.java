package com.brainmagic.fumigation;
import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;

import java.util.ArrayList;
import java.util.List;

import logout.Logout;

public class VisitReportFragment extends AppCompatActivity {
    private Toolbar toolbar;
    private TabLayout tabLayout;
    CustomViewPager viewPager;
    private ViewPagerAdapter adapter;
    private ImageView menu;
    private  String value;
    ViewReportFilesFragment v;
    private LinearLayout containertab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visit_report_fragment2);
        menu=(ImageView)findViewById(R.id.menu);
        containertab=(LinearLayout) findViewById(R.id.containertab);
        v=new ViewReportFilesFragment();

        menu.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view) {
                PopupMenu popupMenu = new PopupMenu(VisitReportFragment.this, view);
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu pop) {
                        pop.dismiss();
                    }
                });
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {

                        switch (item.getItemId()) {
                            case R.id.menuLogout:
                                new Logout(VisitReportFragment.this).log_out();
                                return true;
                            case R.id.changepassword:
                                Intent i2=new Intent(getApplicationContext(), ChangePasswordActivity.class);
                                startActivity(i2);
                                return true;
                            case R.id.attendance:
                                Intent i3=new Intent(getApplicationContext(), AttendanceViewNewActivity.class);
                                startActivity(i3);
                                return true;
                            case R.id.jobcard:
                                Intent i4=new Intent(getApplicationContext(), Jobcarddetails.class);
                                startActivity(i4);
                                return true;
                            case R.id.viewvisit:
                                Intent i5=new Intent(getApplicationContext(), ViewReportActivity.class);
                                startActivity(i5);
                                return true;
                            case  R.id.home:
                                Intent i6=new Intent(getApplicationContext(), SupervisorHomeActivity.class);
                                startActivity(i6);
                                return true;


                        }
                        return false;
                    }
                });

                popupMenu.inflate(R.menu.menuvisit);
                popupMenu.show();
                ;
            }
        });
       // toolbar = (Toolbar) findViewById(R.id.toolbar);
      //  setSupportActionBar(toolbar);

       // getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (CustomViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        viewPager.setPagingEnabled(false);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        LinearLayout l=((LinearLayout)tabLayout.getChildAt(0));
        l.setEnabled(false);
        for(int i=0;i< l.getChildCount();i++){
            l.getChildAt(i).setClickable(false);
        }
//    tabLayout.setOnTouchListener(new View.OnTouchListener() {
//        @Override
//        public boolean onTouch(View view, MotionEvent motionEvent) {
//            return false;
//        }
//    });
        //movew_Forward();
    }


    public void next() {
        viewPager.setCurrentItem(getItemofviewpager(+1), true);
        //movew_Forward();
    }

    public void value(String s1, String s2,String s3, String s4, String s5){
        this.value= s1;
        this.value= s2;
        this.value= s3;
        this.value= s4;
        this.value= s5;
        v.Value(s1,s2,s3,s4,s5);
    }

    private int getItemofviewpager(int i) {
        return viewPager.getCurrentItem() + i;
    }
    private void movew_Forward() {
        adapter = new ScreenSlidePagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setPagingEnabled(false);
        viewPager.setOffscreenPageLimit(1);
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ViewReportFragment(),"Visit Report");
        adapter.addFragment(v, "Add Files");

        viewPager.setAdapter(adapter);

    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }

    private class ScreenSlidePagerAdapter extends ViewPagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager supportFragmentManager) {
            super(supportFragmentManager);
        }
    }
}
