package api.models.workreport1.resposnse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Data{
  @SerializedName("emp")
  @Expose
  private Emp emp;
  public void setEmp(Emp emp){
   this.emp=emp;
  }
  public Emp getEmp(){
   return emp;
  }
}