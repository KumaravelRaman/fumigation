package api.models;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * Awesome Pojo Generator
 * */
public class Imei {
  @SerializedName("MobileNo")
  @Expose
  private Long MobileNo;
  @SerializedName("Zip")
  @Expose
  private Integer Zip;
  @SerializedName("EmailId")
  @Expose
  private String EmailId;
  @SerializedName("UserName")
  @Expose
  private String UserName;
  @SerializedName("Address")
  @Expose
  private String Address;
  @SerializedName("InsertDate")
  @Expose
  private String InsertDate;
  @SerializedName("SupervisorName")
  @Expose
  private String SupervisorName;
  @SerializedName("MobileNumber1")
  @Expose
  private Long MobileNumber1;
  @SerializedName("MobileNumber2")
  @Expose
  private Long MobileNumber2;
  @SerializedName("City")
  @Expose
  private String City;
  @SerializedName("Count")
  @Expose
  private Object Count;
  @SerializedName("StuffingPoint")
  @Expose
  private String StuffingPoint;
  @SerializedName("Name")
  @Expose
  private String Name;
  @SerializedName("UpdateDate")
  @Expose
  private String UpdateDate;
  @SerializedName("Verified")
  @Expose
  private String Verified;
  @SerializedName("IMINumber2")
  @Expose
  private Long IMINumber2;
  @SerializedName("IMINumber1")
  @Expose
  private Long IMINumber1;
  @SerializedName("State")
  @Expose
  private String State;
  @SerializedName("id")
  @Expose
  private Integer id;
  @SerializedName("UserType")
  @Expose
  private String UserType;
  @SerializedName("Password")
  @Expose
  private String Password;
  public void setMobileNo(Long MobileNo){
   this.MobileNo=MobileNo;
  }
  public Long getMobileNo(){
   return MobileNo;
  }
  public void setZip(Integer Zip){
   this.Zip=Zip;
  }
  public Integer getZip(){
   return Zip;
  }
  public void setEmailId(String EmailId){
   this.EmailId=EmailId;
  }
  public String getEmailId(){
   return EmailId;
  }
  public void setUserName(String UserName){
   this.UserName=UserName;
  }
  public String getUserName(){
   return UserName;
  }
  public void setAddress(String Address){
   this.Address=Address;
  }
  public String getAddress(){
   return Address;
  }
  public void setInsertDate(String InsertDate){
   this.InsertDate=InsertDate;
  }
  public String getInsertDate(){
   return InsertDate;
  }
  public void setSupervisorName(String SupervisorName){
   this.SupervisorName=SupervisorName;
  }
  public String getSupervisorName(){
   return SupervisorName;
  }
  public void setMobileNumber1(Long MobileNumber1){
   this.MobileNumber1=MobileNumber1;
  }
  public Long getMobileNumber1(){
   return MobileNumber1;
  }
  public void setMobileNumber2(Long MobileNumber2){
   this.MobileNumber2=MobileNumber2;
  }
  public Long getMobileNumber2(){
   return MobileNumber2;
  }
  public void setCity(String City){
   this.City=City;
  }
  public String getCity(){
   return City;
  }
  public void setCount(Object Count){
   this.Count=Count;
  }
  public Object getCount(){
   return Count;
  }
  public void setStuffingPoint(String StuffingPoint){
   this.StuffingPoint=StuffingPoint;
  }
  public String getStuffingPoint(){
   return StuffingPoint;
  }
  public void setName(String Name){
   this.Name=Name;
  }
  public String getName(){
   return Name;
  }
  public void setUpdateDate(String UpdateDate){
   this.UpdateDate=UpdateDate;
  }
  public String getUpdateDate(){
   return UpdateDate;
  }
  public void setVerified(String Verified){
   this.Verified=Verified;
  }
  public String getVerified(){
   return Verified;
  }
  public void setIMINumber2(Long IMINumber2){
   this.IMINumber2=IMINumber2;
  }
  public Long getIMINumber2(){
   return IMINumber2;
  }
  public void setIMINumber1(Long IMINumber1){
   this.IMINumber1=IMINumber1;
  }
  public Long getIMINumber1(){
   return IMINumber1;
  }
  public void setState(String State){
   this.State=State;
  }
  public String getState(){
   return State;
  }
  public void setId(Integer id){
   this.id=id;
  }
  public Integer getId(){
   return id;
  }
  public void setUserType(String UserType){
   this.UserType=UserType;
  }
  public String getUserType(){
   return UserType;
  }
  public void setPassword(String Password){
   this.Password=Password;
  }
  public String getPassword(){
   return Password;
  }
}