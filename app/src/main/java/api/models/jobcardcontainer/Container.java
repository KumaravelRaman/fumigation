
package api.models.jobcardcontainer;

import javax.annotation.Generated;
import com.google.gson.annotations.SerializedName;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class Container {

    @SerializedName("Audio")
    private String mAudio;
    @SerializedName("AudioContentType")
    private String mAudioContentType;
    @SerializedName("AudioData")
    private String mAudioData;
    @SerializedName("chemicals1")
    private String mChemicals1;
    @SerializedName("ContainerNo")
    private String mContainerNo;
    @SerializedName("id")
    private String mId;
    @SerializedName("Image")
    private String mImage;
    @SerializedName("Image1")
    private String mImage1;
    @SerializedName("Image2")
    private String mImage2;
    @SerializedName("Images")
    private String mImages;
    @SerializedName("Insertdate")
    private String mInsertdate;
    @SerializedName("jobcardid")
    private String mJobcardid;
    @SerializedName("LorryDriverMobNo")
    private String mLorryDriverMobNo;
    @SerializedName("LorryDriverName")
    private String mLorryDriverName;
    @SerializedName("Quatity")
    private String mQuatity;
    @SerializedName("Remarks")
    private String mRemarks;
    @SerializedName("Status")
    private String mStatus;
    @SerializedName("Updatedate")
    private String mUpdatedate;
    @SerializedName("VehicleNo")
    private String mVehicleNo;
    @SerializedName("vhid")
    private String mVhid;
    @SerializedName("warid")
    private String mWarid;
    @SerializedName("workreport")
    private String mWorkreport;

    public String getAudio() {
        return mAudio;
    }

    public void setAudio(String audio) {
        mAudio = audio;
    }

    public String getAudioContentType() {
        return mAudioContentType;
    }

    public void setAudioContentType(String audioContentType) {
        mAudioContentType = audioContentType;
    }

    public String getAudioData() {
        return mAudioData;
    }

    public void setAudioData(String audioData) {
        mAudioData = audioData;
    }

    public String getChemicals1() {
        return mChemicals1;
    }

    public void setChemicals1(String chemicals1) {
        mChemicals1 = chemicals1;
    }

    public String getContainerNo() {
        return mContainerNo;
    }

    public void setContainerNo(String containerNo) {
        mContainerNo = containerNo;
    }

    public String getId() {
        return mId;
    }

    public void setId(String id) {
        mId = id;
    }

    public String getImage() {
        return mImage;
    }

    public void setImage(String image) {
        mImage = image;
    }

    public String getImage1() {
        return mImage1;
    }

    public void setImage1(String image1) {
        mImage1 = image1;
    }

    public String getImage2() {
        return mImage2;
    }

    public void setImage2(String image2) {
        mImage2 = image2;
    }

    public String getImages() {
        return mImages;
    }

    public void setImages(String images) {
        mImages = images;
    }

    public String getInsertdate() {
        return mInsertdate;
    }

    public void setInsertdate(String insertdate) {
        mInsertdate = insertdate;
    }

    public String getJobcardid() {
        return mJobcardid;
    }

    public void setJobcardid(String jobcardid) {
        mJobcardid = jobcardid;
    }

    public String getLorryDriverMobNo() {
        return mLorryDriverMobNo;
    }

    public void setLorryDriverMobNo(String lorryDriverMobNo) {
        mLorryDriverMobNo = lorryDriverMobNo;
    }

    public String getLorryDriverName() {
        return mLorryDriverName;
    }

    public void setLorryDriverName(String lorryDriverName) {
        mLorryDriverName = lorryDriverName;
    }

    public String getQuatity() {
        return mQuatity;
    }

    public void setQuatity(String quatity) {
        mQuatity = quatity;
    }

    public String getRemarks() {
        return mRemarks;
    }

    public void setRemarks(String remarks) {
        mRemarks = remarks;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String status) {
        mStatus = status;
    }

    public String getUpdatedate() {
        return mUpdatedate;
    }

    public void setUpdatedate(String updatedate) {
        mUpdatedate = updatedate;
    }

    public String getVehicleNo() {
        return mVehicleNo;
    }

    public void setVehicleNo(String vehicleNo) {
        mVehicleNo = vehicleNo;
    }

    public String getVhid() {
        return mVhid;
    }

    public void setVhid(String vhid) {
        mVhid = vhid;
    }

    public String getWarid() {
        return mWarid;
    }

    public void setWarid(String warid) {
        mWarid = warid;
    }

    public String getWorkreport() {
        return mWorkreport;
    }

    public void setWorkreport(String workreport) {
        mWorkreport = workreport;
    }

}
