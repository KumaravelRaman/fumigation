package register;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;


import com.brainmagic.fumigation.R;

import java.sql.ResultSet;
import java.sql.Statement;


import alert.Alert;
import api.models.forgot.ForgotPasswordData;
import api.retrofit.APIService;

import api.retrofit.RetrofitClient;
import network.NetworkConnection;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by SYSTEM10 on 4/11/2018.
 */

public class ForgetPasswordAlert {
    Context context;
    EditText Email;
    Button send,cancel;
    Alert box;
    //private SpotsDialog progressDialog;

    ProgressDialog progressDialog1,progressDialog2;
    Statement stmt;
    ResultSet resultSet;
    String Name,UserName,Password,Enteredemail,cuscode;
    SharedPreferences myshare;
    private SharedPreferences.Editor editor;

    public ForgetPasswordAlert(Context con) {
        // TODO Auto-generated constructor stub
        this.context = con;
        this.box = new Alert(context);
    }

    //editor = myshare.edit();
    public void showLoginbox()
    {

        myshare = context.getSharedPreferences("Registration", MODE_PRIVATE);
        editor = myshare.edit();
        cuscode=myshare.getString("customercode","");
        final AlertDialog alertDialog = new AlertDialog.Builder(
                context).create();
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
       View dialogView = inflater.inflate(R.layout.forget_password, null);
        alertDialog.setView(dialogView);
        //TextView log = (TextView) dialogView.findViewById(R.id.);

        Email=(EditText)dialogView.findViewById(R.id.email);
        send =(Button) dialogView.findViewById(R.id.send);
        cancel=(Button) dialogView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Enteredemail = Email.getText().toString();

                if (Enteredemail.equals("")) {
                    Email.setFocusable(true);
                    Email.setError("Enter your  Email ID !");
                }else if(!isValidEmail(Enteredemail.trim()))
                {
                    Email.setFocusable(true);
                    Email.setError("Enter valid Email ID");
                }
                else{
                    CheckInternet();
                    alertDialog.dismiss();
                }
            }
        });

        alertDialog.show();
    }


    private void CheckInternet() {
        try {
            NetworkConnection network = new NetworkConnection(context);
            if (network.CheckInternet()) {
              forgetpassword(Enteredemail);

            } else {
                Alert alert = new Alert(context);
                alert.showAlert("Kindly check your Internet Connection");

            }

        }catch (Exception e) {
            e.printStackTrace();

        }
    }


    void forgetpassword(String email){
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        APIService service = RetrofitClient.getApiService();
        Call<ForgotPasswordData> call = service.forgot(cuscode,email);
        call.enqueue(new Callback<ForgotPasswordData>() {
            @Override
            public void onResponse(Call<ForgotPasswordData> call, Response<ForgotPasswordData> response) {
                progressDialog.dismiss();
                switch (response.body().getResult())
                {
                    case "Success":
                        box.showAlert("Password has been sent to your Email successfully");
                        break;
                    case "MailIdNotFound":
                        box.showAlert("Please enter the registered Email!");
                        break;
                    default: {
                        box.showAlert("Connection interrupted! Please try again");
                        break;
                    }
                }

            }

            @Override
            public void onFailure(Call<ForgotPasswordData> call, Throwable t)
            {
                progressDialog.dismiss();
                box.showAlert("Connection interrupted! Please try again");


            }
      });
   }

    class CheckEmaild  {
        private String namestrng;



    }




    public static boolean isValidEmail(CharSequence target) {
        if (TextUtils.isEmpty(target))
        {
            return false;
        }
        else
        {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

}
